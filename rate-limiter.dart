import 'dart:math';

import 'package:flutter/material.dart';

class RateLimiterTestWidget extends StatefulWidget {
  const RateLimiterTestWidget({Key? key}) : super(key: key);

  @override
  State<RateLimiterTestWidget> createState() => _RateLimiterTestWidgetState();
}

class _RateLimiterTestWidgetState extends State<RateLimiterTestWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              ElevatedButton(
                onPressed: () async {
                  await RateLimiter.send(() async {
                    print('Request 1 : ${await somefunc()}');
                    setState(() {});
                  });
                },
                child: const Text('Request 1'),
              ),
              ElevatedButton(
                onPressed: () async {
                  await RateLimiter.send(() async {
                    print('Request 2 : ${await somefunc()}');
                    setState(() {});
                  });
                },
                child: const Text('Request 2'),
              ),
              ElevatedButton(
                onPressed: () {
                  if (RateLimiter.isLimitsExist) {
                    RateLimiter.clearLimits();
                  } else {
                    RateLimiter.addLimit(rateLimit: RateLimit(timeMs: 1000, requestCount: 1));
                    RateLimiter.addLimit(rateLimit: RateLimit(timeMs: 5000, requestCount: 3));
                    RateLimiter.addLimit(rateLimit: RateLimit(timeMs: 10000, requestCount: 5));
                  }
                  setState(() {});
                },
                child: const Text('SetLimits / ResetLimits'),
              ),
              const Divider(),
              const Text('LIMITS'),
              ...RateLimiter._limits.map((e) => Text('${e.timeMs} = ${e.requestCount}')).toList(),
              const Divider(),
              const Text('REQUESTS'),
              ...RateLimiter._calls.map((e) => Text(e.toString())).toList(),
              const Divider(),
            ],
          ),
        ),
      ),
    );
  }
}

Future<int> somefunc() async {
  int delay = Random().nextInt(100);
  await Future.delayed(Duration(milliseconds: delay));
  return delay;
}

class RateLimit {
  final int timeMs;
  final int requestCount;

  RateLimit({required this.timeMs, required this.requestCount});
}

class RateLimiter {
  static final List<RateLimit> _limits = [];
  static final List<Function> _requests = [];
  static final List<DateTime> _calls = [];

  static addLimit({required RateLimit rateLimit}) {
    _limits.add(rateLimit);
  }

  static bool get isLimitsExist => _limits.isEmpty ? false : true;

  static clearLimits() {
    _limits.clear();
  }

  static void send(Function request) async {
    if (_requests.isEmpty) {
      _requests.insert(0, request);
      while (_requests.isNotEmpty) {
        if (_limits.isNotEmpty) {
          _calls.insert(0, DateTime.now());
          for (RateLimit limit in _limits) {
            if ((_calls.length > limit.requestCount) &&
                (_calls.first.difference(_calls[limit.requestCount]) <
                    Duration(milliseconds: limit.timeMs))) {
              await Future.delayed(Duration(
                  milliseconds: (limit.timeMs -
                      (_calls.first.difference(_calls[limit.requestCount]).inMilliseconds))));
              _calls.first = DateTime.now();
            }
            _calls.first = DateTime.now();
          }
          try {
             _requests.removeLast().call();
          } catch (e) { // Log Exception
            print(e.toString()); // Log Exception
          }
        } else {
          try {
             _requests.removeLast().call();
          } catch (e) {
            print(e.toString()); // Log Exception
          }
        }
        if (_limits.isNotEmpty && _calls.isNotEmpty) {
          int index = _limits.fold(
              0,
              (previousValue, element) =>
                  element.requestCount > previousValue ? element.requestCount : previousValue);
          if (_calls.length > index) _calls.removeAt(index);
        } else {
          _calls.clear();
        }
      }
    } else {
      _requests.insert(0, request);
    }
    return;
  }
}
